# springcloud-idea-demo1

#### 介绍


小组成员：（2-5人）

学号+姓名  

角色

### 一、实验目的

高可用注册中心实验模板，T1分支为简版

### 二、实验仪器设备与环境

PC机 windows 10,2-5台

IDEA 2019 +  **JDK1.8 + SpringCloud GreenWich SR** 

添加部署图

![输入图片说明](doc/image.png)

### 三、实验原理

Eureka Server + Eureka Client

### 四、实验内容与步骤

1. 配置Maven环境  
2. 新建项目  
3. 创建码云项目  
4. 小组克隆项目  
5. 根据角色修改配置文件
 

### 五、实验结果与分析

最终实验结果界面

当EurekaServer1或者EurekaServer2停机后，访问另一个EurekaServer会有什么效果？ 

### 六、结论与体会

通过这次实验，理解了高可用。

 


